import CreateML


import Foundation
import PlaygroundSupport

do {
    let data = try MLDataTable(contentsOf: URL(fileURLWithPath: "/Users/apple/Documents/Shared Playground Data/nlpJson.json"))
    
    let (trainingData, testingData) = data.randomSplit(by: 0.8, seed: 5)
    let wordTagger = try MLWordTagger(trainingData: trainingData,
    tokenColumn: "tokens",
    labelColumn: "labels")
    
    // Training accuracy as a percentage
    let trainingAccuracy = (1.0 - wordTagger.trainingMetrics.taggingError) * 100

    // Validation accuracy as a percentage
    let validationAccuracy = (1.0 - wordTagger.validationMetrics.taggingError) * 100
    
    
    print("Training and Validation Accuracy",trainingAccuracy, validationAccuracy)
    
    let evaluationMetrics = wordTagger.evaluation(on: testingData,
    tokenColumn: "tokens",
    labelColumn: "labels")
    
    
    let evaluationAccuracy = (1.0 - evaluationMetrics.taggingError) * 100

    print("Evaluation Accuracy", evaluationAccuracy)
    
    let metadata = MLModelMetadata(author: "Raza Anis",
                                   shortDescription: "A model trained to tag entities in search text using data given in the email.",
                                   version: "3.0")

    try wordTagger.write(to: URL(fileURLWithPath: "/Users/apple/Documents/Shared Playground Data/nlpComplete"),
                                  metadata: metadata)
    
} catch {
    print(error)
}
